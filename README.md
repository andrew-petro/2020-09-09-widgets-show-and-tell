# 2020-09-09 WPS Show-and-Tell wrt MyUW widgets

2020-09-09 WPS Show and Tell wrt MyUW widgets.


## Summary

+ MyUW widgets combine a template, configuration, and data
  to render a pithy box.


## Objectives of this talk

After this talk, you'll be able to

+ Explain what a MyUW widget is
+ Explain widget architecture, and how templates, configuration,
  and dynamic data feed into it. Appreciate the loose coupling.
+ Give a few examples of MyUW widgets

## Background



## What's a widget

Widgets consist of a template, configuration, and dynamic data.

+ template: AngularJS template, usually as abstracted into a widget type
+ configuration: JSON
+ dynamic data: URL, returning JSON or RSS

## Examples of widgets

### No widget - just a hyperlink

Example: Box

+ [Box in app directory](https://my.wisc.edu/web/apps/details/box)
+ [Box entity file](https://git.doit.wisc.edu/myuw-overlay/entities/-/blob/prod/src/main/resources/portlet-definition/box.portlet-definition.xml)

### List of Links

Example: Voting

+ [Voting in app directory](https://my.wisc.edu/web/apps/details/voting)
+ [Voting entity file](https://git.doit.wisc.edu/myuw-overlay/entities/-/blob/prod/src/main/resources/portlet-definition/voting.portlet-definition.xml)

Example: Payroll Information

The links on the widget can be dynamic,
sourced from JSON at a URL rather than from configuration.

+ [Payroll Information in app directory](https://my.wisc.edu/web/apps/details/earnings-statement)
+ [Payroll Information entity file](https://git.doit.wisc.edu/myuw-overlay/entities/-/blob/prod/src/main/resources/portlet-definition/earnings-statement.portlet-definition.xml)
+ [Dynamic JSON](https://my.wisc.edu/portal/p/earnings-statement.ctf2/max/listOfLinks.resource.uP)

### Other widget types

There are more.

<https://uportal-project.github.io/uportal-app-framework/make-a-widget.html>

### Custom

Wiscard balance

+ [Wiscard balance in app directory](https://my.wisc.edu/web/apps/details/wiscard-balance)
+ [Wiscard entity file](https://git.doit.wisc.edu/myuw-overlay/entities/-/blob/prod/src/main/resources/portlet-definition/wiscard.portlet-definition.xml)

Note that the loose coupling means I have no idea how the Wiscard vendor is
generating that JSON. .NET? ColdFusion? It doesn't matter.

Separating the "view" from the "controller" for the win.

Preparing for SOAR

SOAR Orientation

### Remote

Benefit information

Technically, the widget can draw the fully rendered content from a URL.

This means a legacy JSR-286 Portlet can provide arbitrary markup for a widget.
We use this for the Benefit Information widget and its many states supporting
benefit enrollment.

(Yes, PHP, WordPress, Salesforce, or your other server-side solutiuon of choice
could, technically, be generating arbitrarily awesome widgets.)

Note the tradeoffs here.

## Let's make a widget

### Example 1: Enhancing the DoITNet widget

Did you know there's a DoITNet widget?
You can pin DoITNet from your MyUW home page.

But it might be even cooler if you could search DoITNet.

So, it needs a widget type, and configuration.

```xml
   <portlet-preference>
      <name>widgetType</name>
      <value>search-with-links</value>
    </portlet-preference>
    <portlet-preference>
      <name>widgetConfig</name>
      <value><![CDATA[
             {
               "actionURL":"https://doitnet.doit.wisc.edu/",
               "actionTarget":"_blank",
               "actionParameter":"s",
               "launchText":"Open website",
               "links":[
                  {
                     "title":"Org chart",
                     "href":"https://it.wisc.edu/wp-content/uploads/Updated-Org-Chart-Master-1.pdf",
                     "icon":"fa-map-o",
                     "target":"_blank"
                  }
               ]
            }]]></value>
    </portlet-preference>
```

Let's show doing this in source and running the entity import Jenkins job...

### Example 2: WiscIT incident launcher

Let's show importing this entity.

## Characteristics of this architecture

+ Shared templates via widget types adds abstraction, and leverage.
  + Types make similar content look and behave similarly
  + Improve the type, realize the improvement everywhere it's used.
+ Separation of concerns - Model-View-Controller-ish
  + Model: configuration, dynamic data
  + View: template. Styling. Markup.
  + Controller: type
+ As implemented, with 2020 hindsight
  + The widget types and especially the templates have too much "business logic"
    in them. The AngularJS directives feel complicated. It might be nice to
    have more pure "views" that are more straight tranformations to markup.

## Beyond AngularJS

Web Components? (This may be a chance to refactor to lighter views )
Design System?

Widgets beyond home page: <https://my.wisc.edu/Lumen/>

## Conclusions


## Appendix

Links to docs


